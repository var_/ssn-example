package com.var.ai.ssn;

public class MNISTMultiLayerNetworkConfiguration {

	//number of rows and columns in the input pictures
	static final int NUM_ROWS = 28;
	static final int NUM_COLUMNS = 28;
	static final int OUTPUT_NUM = 10; // number of output classes
	static final int RNG_SEED = 123; // random number seed for reproducibility
	static final int NUM_EPOCHS = 15; // number of epochs to perform
	static final int BATCH_SIZE = 64; // batch size for each epoch
	static final double RATE = 0.0015; // learning rate

}
