package com.var.ai.ssn;


import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.NUM_COLUMNS;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.NUM_ROWS;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.OUTPUT_NUM;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.RATE;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.RNG_SEED;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run( Application.class, args );
	}

	@Bean
	public MultiLayerConfiguration multiLayerConfiguration() {
		return new NeuralNetConfiguration.Builder()
				.seed( RNG_SEED ) //include a random seed for reproducibility
				.activation( Activation.RELU )
				.weightInit( WeightInit.XAVIER )
				.updater( new Nesterovs( RATE, 0.98 ) )
				.l2( RATE * 0.005 ) // regularize learning model
				.list()
				.layer( 0, new DenseLayer.Builder() //create the first input layer.
						.nIn( NUM_ROWS * NUM_COLUMNS )
						.nOut( 500 )
						.build() )
				.layer( 1, new DenseLayer.Builder() //create the second input layer
						.nIn( 500 )
						.nOut( 100 )
						.build() )
				.layer(
						2,
						new OutputLayer.Builder( LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD ) //create hidden layer
								.activation( Activation.SOFTMAX )
								.nIn( 100 )
								.nOut( OUTPUT_NUM )
								.build()
				)
				.pretrain( false ).backprop( true ) //use backpropagation to adjust weights
				.build();
	}
}