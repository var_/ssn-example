package com.var.ai.ssn;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class DigitRecognitionSSNController {

	private final MNISTMultiLayerNetwork network;

	@Autowired
	public DigitRecognitionSSNController(MNISTMultiLayerNetwork network) {
		this.network = network;
	}

	@RequestMapping("/")
	public String recognize(@RequestParam(name = "file") MultipartFile[] files) throws IOException {
		StringBuilder resultBuilder = new StringBuilder( "[" );
		for(MultipartFile file  : files) {
			resultBuilder.append( network.recognize( file.getInputStream() ));
			resultBuilder.append( " ");
		}
		resultBuilder.append( "]" );
		return resultBuilder.toString();
	}


}