package com.var.ai.ssn;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.cpu.nativecpu.NDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.springframework.stereotype.Component;

import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.BATCH_SIZE;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.NUM_EPOCHS;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.OUTPUT_NUM;
import static com.var.ai.ssn.MNISTMultiLayerNetworkConfiguration.RNG_SEED;

@Component
public class MNISTMultiLayerNetwork {

	private static final Logger LOGGER = LoggerFactory.getLogger( MNISTMultiLayerNetwork.class );
	private final MultiLayerNetwork network;

	public MNISTMultiLayerNetwork(MultiLayerConfiguration configuration) {
		this.network = new MultiLayerNetwork( configuration );
	}

	@PostConstruct
	public void init() throws IOException {
		//Get the DataSetIterators:
		DataSetIterator mnistTrain = new MnistDataSetIterator( BATCH_SIZE, true, RNG_SEED );
		DataSetIterator mnistTest = new MnistDataSetIterator( BATCH_SIZE, false, RNG_SEED );

		network.init();
		network.setListeners( new ScoreIterationListener( 5 ) );  //print the score with every iteration

		LOGGER.info( "Train model...." );
		for ( int i = 0; i < NUM_EPOCHS; i++ ) {
			LOGGER.info( "Epoch " + i );
			network.fit( mnistTrain );
		}


		LOGGER.info( "Evaluate model...." );
		Evaluation eval = new Evaluation( OUTPUT_NUM ); //create an evaluation object with 10 possible classes
		while ( mnistTest.hasNext() ) {
			DataSet next = mnistTest.next();
			INDArray output = network.output( next.getFeatureMatrix() ); //get the networks prediction
			eval.eval( next.getLabels(), output ); //check the prediction against the true class
		}

		LOGGER.info( eval.stats() );
	}

	public int recognize(InputStream img) throws IOException {
		BufferedImage bufferedImage = ImageIO.read( img );
		NDArray baseNDArray = new NDArray( getFloats( bufferedImage ) );

		INDArray result = network.output( baseNDArray ).getRow( 0 );
		int prediction = -1;
		float highest = 0;
		StringBuilder sb = new StringBuilder( ". " );
		for ( int i = 0; i < result.columns(); i++ ) {
			Float element = (Float) result.getColumn( i ).element();
			if ( highest < element ) {
				highest = element;
				prediction = i;
			}
			sb.append( new DecimalFormat( "#.##" ).format( element ) + ". " );
		}

		return prediction;
	}

	public static float[] getFloats(BufferedImage image) {
		float[] img = new float[image.getHeight() * image.getWidth()];
		int j = 0;
		for ( int y = 0; y < image.getHeight(); y++ ) {
			for ( int x = 0; x < image.getWidth(); x++ ) {
				int color = image.getRGB( x, y );
				float luminance = 1 - ( ( color & 255 ) / 255F );
				img[j++] = luminance;
			}
		}
		return img;
	}
}
